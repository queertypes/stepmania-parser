# FILES: 2475
      FIELD_NAME | THERE? EMPTY? OPTIONAL?
---------------------------------------------------
           TITLE |   2475      0         N
        SUBTITLE |   2475   1619         Y
          ARTIST |   2475      1         Y
   TITLETRANSLIT |   2475   2459         Y
SUBTITLETRANSLIT |   2475   2474         Y
  ARTISTTRANSLIT |   2475   2451         Y
           GENRE |   2106   1473         Y
          CREDIT |   2475   1595         Y
          BANNER |   2473    422         Y
      BACKGROUND |   2472    498         Y
      LYRICSPATH |   2475   2464         Y
         CDTITLE |   2473   1922         Y
           MUSIC |   2475      0         N
          OFFSET |   2476      0         N
     SAMPLESTART |   2475      0         N
    SAMPLELENGTH |   2475      0         N
      SELECTABLE |   2473      0         Y
            BPMS |   2475      0         N
      DISPLAYBPM |    914      0         Y
           STOPS |   2473   1685         Y
       BGCHANGES |   2423   2167         Y
       KEYSOUNDS |   2091   2091         Y
