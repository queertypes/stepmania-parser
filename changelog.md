# 0.2.1.0 (March 30, 2017)

* full header parsing (no tests)
* BPM changes parsing (no tests)
* song stops parsing (no tests)

# 0.2.0.0 (March 30, 2017)

* migrate to using attoparsec

# 0.1.1.0 (December 15, 2016)

* Add base types for sm file

# 0.1.0.0 (December 15, 2016)

* Project start
