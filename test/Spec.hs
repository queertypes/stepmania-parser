{-# OPTIONS_GHC -Wall #-}
module Main where

import YNotPrelude

import Data.Attoparsec.ByteString
import Test.Tasty
import Test.Tasty.Hspec
import qualified Data.Text.Encoding as TE
import qualified Data.Text as T

import Stepmania.Parser
import Stepmania.Types hiding (Left, Right)

parseMaybe :: Parser a -> ByteString -> Maybe a
parseMaybe p b = case parseOnly p b of
  (Left _) -> Nothing
  (Right a) -> Just a

parserSpecs :: Spec
parserSpecs = do
  describe "Stepmania.Parser" $ do
    it "parses titles" $ do
      parseMaybe parseTitle (TE.encodeUtf8 . T.pack $ "#TITLE:Cats Are Great;")
        `shouldBe` (Just ( Title (T.pack "Cats Are Great")))

    it "parses unicode titles" $ do
      parseMaybe parseTitle (TE.encodeUtf8 . T.pack $ "#TITLE:女 女 女;")
        `shouldBe` (Just (Title (T.pack "女 女 女")))

    it "parses subtitles" $ do
      parseMaybe parseSubtitle (TE.encodeUtf8 . T.pack $ "#SUBTITLE:Most Days;")
        `shouldBe` (Just (Subtitle (T.pack "Most Days")))

    it "parses artists" $ do
      parseMaybe parseArtist (TE.encodeUtf8 . T.pack $ "#ARTIST:Warm Fuzzy;")
        `shouldBe` (Just (Artist (T.pack "Warm Fuzzy")))

main :: IO ()
main = do
  specs <- testSpec "parser specs" parserSpecs
  defaultMain specs
