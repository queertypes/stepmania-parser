#!/bin/bash

set -uo pipefail
IFS=$'\n\t'

declare -ir FILE_CNT=$(find . -name '*.sm' -type f | wc -l)
echo "# FILES: $FILE_CNT"
printf "%16s | %6s %6s %9s\n" "FIELD_NAME" "THERE?" "EMPTY? OPTIONAL?"
echo "---------------------------------------------------"

declare -i EMPTY_FIELD
declare -i FIELD_THERE
declare OPTIONAL

for i in {TITLE,SUBTITLE,ARTIST,TITLETRANSLIT,SUBTITLETRANSLIT,ARTISTTRANSLIT,GENRE,CREDIT,BANNER,BACKGROUND,LYRICSPATH,CDTITLE,MUSIC,OFFSET,SAMPLESTART,SAMPLELENGTH,SELECTABLE,BPMS,DISPLAYBPM,STOPS,BGCHANGES,KEYSOUNDS}
  do EMPTY_FIELD=$(find . -name '*.sm' -type f -print0 | xargs -0 grep "#$i:;" | wc -l)
     FIELD_THERE=$(find . -name '*.sm' -type f -print0 | xargs -0 grep "#$i:" | wc -l)
     OPTIONAL=$(if [ "$EMPTY_FIELD" -eq 0 ] && [ "$FIELD_THERE" -ge "$FILE_CNT" ]; then echo "N"; else echo "Y"; fi;)
     printf "%16s | %6d %6d %9s\n" "$i" "$FIELD_THERE" "$EMPTY_FIELD" "$OPTIONAL"
done
