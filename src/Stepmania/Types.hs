{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
module Stepmania.Types (
  Seconds(..),
  Milliseconds(..),
  BPM(..),
  MeasureLength(..),
  BeatNumber(..),
  BPMSpeed(..),
  Stop(..),

  Title(..),
  Subtitle(..),
  Artist(..),
  TitleTranslit(..),
  SubtitleTranslit(..),
  ArtistTranslit(..),
  Genre(..),
  Credit(..),
  BannerPath(..),
  BackgroundPath(..),
  LyricsPath(..),
  CDTitlePath(..),
  MusicPath(..),
  Offset(..),
  SongBPM(..),
  Stops(..),
  SampleStart(..),
  SampleLength(..),

  SongData(..),
  Header(..),
  NotePos(..),
  Note(..),
  leftNote,
  rightNote,
  upNote,
  downNote,

  NoteLine(..),
  Measure(..),
  NoteData(..),
  Notes(..),
  DisplayBPM(..),
  Selectable(..),
  DisplayChangeSpec,
  BGChanges(..),
  FGChanges(..),
  ChartType,
  Description(..),
  Author(..),

  Difficulty(..),
  Meter(..),
  GrooveRadar,
  NoteType(..),
  BeatType(..)
) where


import Prelude hiding (Left, Right)

import Data.Text (Text)
import Data.Word (Word8, Word32)
import Data.Int (Int32)

newtype Seconds = Seconds Int32 deriving (Show, Eq)  -- negative seconds allowed
newtype Milliseconds = Milliseconds Double deriving (Show, Eq)
newtype BPMSpeed = BPMSpeed Double deriving (Show, Eq) -- negative BPMs allowed
newtype MeasureLength = MeasureLength Word8 deriving (Show, Eq) -- maximum measure length is 192
newtype BeatNumber = BeatNumber Double deriving (Show, Eq)
newtype Stop = Stop (BeatNumber, Milliseconds) deriving (Show, Eq)
newtype BPM = BPM (BeatNumber, BPMSpeed) deriving (Show, Eq)

newtype Title = Title Text deriving (Show, Eq)
newtype Subtitle = Subtitle Text deriving (Show, Eq)
newtype Artist = Artist Text deriving (Show, Eq)
newtype TitleTranslit = TitleTranslit Text deriving (Show, Eq)
newtype SubtitleTranslit = SubtitleTranslit Text deriving (Show, Eq)
newtype ArtistTranslit = ArtistTranslit Text deriving (Show, Eq)
newtype Genre = Genre Text deriving (Show, Eq)
newtype Credit = Credit Text deriving (Show, Eq)
newtype BannerPath = BannerPath FilePath deriving (Show, Eq)
newtype BackgroundPath = BackgroundPath FilePath deriving (Show, Eq)
newtype LyricsPath = LyricsPath FilePath deriving (Show, Eq)
newtype CDTitlePath = CDTitlePath FilePath deriving (Show, Eq)
newtype MusicPath = MusicPath FilePath deriving (Show, Eq)
newtype Offset = Offset (Seconds, Milliseconds) deriving (Show, Eq)
newtype SongBPM = SongBPM [BPM] deriving (Show, Eq)
newtype Stops = Stops [Stop] deriving (Show, Eq)
newtype SampleStart = SampleStart (Seconds, Milliseconds) deriving (Show, Eq)
newtype SampleLength = SampleLength (Seconds, Milliseconds) deriving (Show, Eq)

data SongData
  = SongData { header :: !Header
             , noteData :: !NoteData
             }
  deriving (Show, Eq)

data Header
  = Header { title :: !Title
           , subtitle :: !Subtitle
           , artist :: !Artist
           , titleTranslit :: !TitleTranslit
           , subtitleTranslit :: !SubtitleTranslit
           , artistTranslit :: !ArtistTranslit
           , genre :: !Genre
           , credit :: !Credit
           , bannerPath :: !BannerPath
           , backgroundPath :: !BackgroundPath
           , lyricsPath :: !LyricsPath
           , cdTitlePath :: !CDTitlePath
           , musicPath :: !MusicPath
           , offset :: !Offset
           , songBPMs :: !SongBPM
           , stops :: !Stops
           , sampleStart :: !SampleStart
           , sampleLength :: !SampleLength
           }
 deriving (Show, Eq)

data NotePos
  = Left | Up | Down | Right
 deriving (Show, Eq)

data Note (pos :: NotePos) =
  Note NoteType BeatType
  deriving (Show, Eq)

leftNote :: NoteType -> BeatType -> Note 'Left
rightNote :: NoteType -> BeatType -> Note 'Right
upNote :: NoteType -> BeatType -> Note 'Up
downNote :: NoteType -> BeatType -> Note 'Down
leftNote = Note
rightNote = Note
upNote = Note
downNote = Note

data NoteLine =
  NoteLine { left :: Note 'Left
           , up :: Note 'Up
           , down :: Note 'Down
           , right :: Note 'Right
           }
 deriving (Show, Eq)

data Notes = Notes [Measure]
 deriving (Show, Eq)

data Measure
  = Measure MeasureLength [NoteLine]
 deriving (Show, Eq)

data NoteData
  = NoteData { notes :: !Notes
             , chartType :: !ChartType
             , description :: !Description
             , author :: !Author
             , difficulty :: !Difficulty
             , meter :: !Meter -- 1, 2, 3, .., 21, 22, ... - how hard is it?
             , grooveRadar :: !GrooveRadar
             }
 deriving (Show, Eq)

data DisplayBPM
  = StaticBPM BPM
  | RangeBPM BPM BPM
  | RandomBPM
 deriving (Show, Eq)

newtype Selectable = Selectable Bool deriving (Show, Eq)

data DisplayChangeSpec
newtype BGChanges = BGChanges [DisplayChangeSpec]
newtype FGChanges = FGChanges [DisplayChangeSpec]

data ChartType = Cool deriving (Show, Eq)
newtype Description = Description Text deriving (Show, Eq)
newtype Author = Author Text deriving (Show, Eq)

data Difficulty
  = Beginner
  | Easy
  | Medium
  | Hard
  | Challenge
  | Edit
 deriving (Show, Eq)

newtype Meter = Meter Word32 deriving (Show, Eq)
newtype GrooveRadar = GrooveRadar (Word8,Word8,Word8,Word8,Word8) deriving (Show, Eq)
data NoteType
  = Empty    -- 0
  | Normal   -- 1
  | HoldHead -- 2
  | HoldTail -- 3
  | RollHead -- 4
  | Mine     -- M
  | Keysound -- K
  | Lift     -- L
  | Fake     -- F
 deriving (Show, Eq)

data BeatType
  = Beat4
  | Beat8
  | Beat12
  | Beat16
  | Beat24
  | Beat32
  | Beat48
  | Beat64
  | Beat192
 deriving (Show, Eq)
