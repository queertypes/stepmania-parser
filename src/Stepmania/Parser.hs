{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wall #-}
module Stepmania.Parser (
  parseArtist,
  parseArtistTranslit,
  parseBGChanges,
  parseBPMChanges,
  parseBackground,
  parseBanner,
  parseCDTitle,
  parseCredit,
  parseGenre,
  -- parseKeySounds,
  parseLyricsPath,
  parseMusic,
  parseNotes,
  parseOffset,
  parseSampleLength,
  parseSampleStart,
  parseSelectable,
  parseStops,
  parseSubtitle,
  parseSubtitleTranslit,
  parseTitle,
  parseTitleTranslit
) where

import YNotPrelude hiding (signed, decimal, double)

import Data.Attoparsec.ByteString
import Data.Attoparsec.ByteString.Char8 hiding (takeWhile1)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE

import Stepmania.Types

--------------------------------------------------------------------------------
--                              Parser                                        --
--------------------------------------------------------------------------------
colon :: Parser Word8
colon = word8 58

semicolon :: Parser Word8
semicolon = word8 59

pound :: Parser Word8
pound = word8 35

dot :: Parser Word8
dot = word8 46

equalSign :: Parser Word8
equalSign = word8 61

comma :: Parser Word8
comma = word8 44

eol :: Parser ()
eol = endOfLine

parseTag :: ByteString -> Parser ByteString
parseTag tag =
  pound *> stringCI tag *> colon *> takeWhile1 (/= 59) <* semicolon

-- |
-- all valid tag names
-- in place to localize spelling errors to the `show` instance
data TagName
  = TNTitle
  | TNSubtitle
  | TNArtist
  | TNTitleTranslit
  | TNSubtitleTranslit
  | TNArtistTranslit
  | TNGenre
  | TNCredit
  | TNBanner
  | TNBackground
  | TNLyricsPath
  | TNCDTitle
  | TNMusic
  | TNOffset
  | TNSampleStart
  | TNSampleLength
  | TNSelectable
  | TNBPMs
  | TNStops
  | TNBgChanges
  | TNKeySounds
  | TNNotes

showTagName :: TagName -> Text
showTagName =
  \case
    TNTitle -> "title"
    TNSubtitle -> "subtitle"
    TNArtist -> "artist"
    TNTitleTranslit -> "titletranslit"
    TNSubtitleTranslit -> "subtitletranslit"
    TNArtistTranslit -> "artisttranslit"
    TNGenre -> "genre"
    TNCredit -> "credit"
    TNBanner -> "banner"
    TNBackground -> "background"
    TNLyricsPath -> "lyricspath"
    TNCDTitle -> "cdtitle"
    TNMusic -> "music"
    TNOffset -> "offset"
    TNSampleStart -> "samplestart"
    TNSampleLength -> "samplelength"
    TNSelectable -> "selectable"
    TNBPMs -> "bpms"
    TNStops -> "stops"
    TNBgChanges -> "bgchanges"
    TNKeySounds -> "keysounds"
    TNNotes -> "notes"

tagBytes :: TagName -> ByteString
tagBytes = TE.encodeUtf8 . showTagName

-- #TITLE:woot woot;
parseTitle :: Parser Title
parseTitle = Title . TE.decodeUtf8 <$> parseTag (tagBytes TNTitle)

-- #SUBTITLE:a song about cats;
parseSubtitle :: Parser Subtitle
parseSubtitle = Subtitle . TE.decodeUtf8 <$> parseTag (tagBytes TNSubtitle)

-- #ARTIST:cat grrrl;
parseArtist :: Parser Artist
parseArtist = Artist . TE.decodeUtf8 <$> parseTag (tagBytes TNArtist)

-- #TITLETRANSLIT:;
parseTitleTranslit :: Parser TitleTranslit
parseTitleTranslit = TitleTranslit . TE.decodeUtf8 <$> parseTag (tagBytes TNTitleTranslit)

-- #SUBTITLETRANSLIT:;
parseSubtitleTranslit :: Parser SubtitleTranslit
parseSubtitleTranslit =
  SubtitleTranslit . TE.decodeUtf8 <$> parseTag (tagBytes TNSubtitleTranslit)

-- #ARTISTTRANSLIT:;
parseArtistTranslit :: Parser ArtistTranslit
parseArtistTranslit =
  ArtistTranslit . TE.decodeUtf8 <$> parseTag (tagBytes TNArtistTranslit)

-- #GENRE:techno;
parseGenre :: Parser Genre
parseGenre = Genre . TE.decodeUtf8 <$> parseTag (tagBytes TNGenre)

-- #CREDIT:cats;
parseCredit :: Parser Credit
parseCredit = Credit . TE.decodeUtf8 <$> parseTag (tagBytes TNCredit)

-- #BANNER:/home/allele/games/stepmania/Songs/good-songs/woot-woot/banner.jpg;
parseBanner :: Parser BannerPath
parseBanner = BannerPath . T.unpack . TE.decodeUtf8 <$> parseTag (tagBytes TNBanner)

-- #BACKGROUND:/home/allele/games/stepmania/Songs/good-songs/woot-woot/bg.jpg;
parseBackground :: Parser BackgroundPath
parseBackground = BackgroundPath . T.unpack . TE.decodeUtf8 <$> parseTag (tagBytes TNBackground)

-- #LYRICSPATH:/home/allele/games/stepmania/Songs/good-songs/woot-woot/lyrics.txt;
parseLyricsPath :: Parser LyricsPath
parseLyricsPath = LyricsPath . T.unpack . TE.decodeUtf8 <$> parseTag (tagBytes TNLyricsPath)

-- #CDTITLE:/home/allele/games/stepmania/Songs/good-songs/woot-woot/cdtitle.txt;
parseCDTitle :: Parser CDTitlePath
parseCDTitle = CDTitlePath . T.unpack . TE.decodeUtf8 <$> parseTag (tagBytes TNCDTitle)

-- #MUSIC:/home/allele/games/stepmania/Songs/good-songs/woot-woot/the music.ogg;
parseMusic :: Parser MusicPath
parseMusic = MusicPath . T.unpack . TE.decodeUtf8  <$> parseTag (tagBytes TNMusic)

signedInt :: Parser Int
signedInt = signed decimal

int :: Parser Int
int = decimal

parseTime :: Parser (Seconds, Milliseconds)
parseTime =
  let seconds = (Seconds . fromIntegral) <$> signedInt
      milliseconds = (Milliseconds . fromIntegral) <$> (dot *> int)
  in (,) <$> seconds <*> milliseconds

parseTimeTag :: TagName -> Parser (Seconds, Milliseconds)
parseTimeTag tn = startTag *> parseTime <* char ';'
  where startTag =
          pound *> string (tagBytes tn) *> char ':'

-- #OFFSET:-2.55;
parseOffset :: Parser Offset
parseOffset = Offset <$> parseTimeTag TNOffset

-- #SAMPLESTART:1.10;
parseSampleStart :: Parser SampleStart
parseSampleStart = SampleStart <$> parseTimeTag TNSampleStart

-- #SAMPLELENGTH:12.000;
parseSampleLength :: Parser SampleLength
parseSampleLength = SampleLength <$> parseTimeTag TNSampleLength

-- #SELECTABLE:YES;
parseSelectable :: Parser Selectable
parseSelectable =
  Selectable <$> (startTag *> parseSelect)
  where startTag :: Parser Char
        startTag = pound *> stringCI (tagBytes TNSelectable) *> char ':'

        parseSelect :: Parser Bool
        parseSelect =
          (stringCI "YES" $> True) <|> (stringCI "NO" $> False)

parseEqSep :: Parser (Double,Double)
parseEqSep = (,) <$> double <*> (equalSign *> double)

parseBPM :: Parser BPM
parseBPM = (\(d1,d2) -> BPM (BeatNumber d1, BPMSpeed d2)) <$> parseEqSep

parseStop :: Parser Stop
parseStop = (\(d1,d2) -> Stop (BeatNumber d1, Milliseconds d2)) <$> parseEqSep

-- #BPMs:0.00=180.000, 120.00=90.000, 150.00=180.00;
parseBPMChanges :: Parser SongBPM
parseBPMChanges =
  SongBPM <$> parseSongBPMs
  where parseSongBPMs =
          pound *> stringCI (tagBytes TNBPMs) *> colon *> sepBy parseBPM comma <* semicolon

-- #STOPS:0.00=180.000, 120.00=90.000, 150.00=180.00;
parseStops :: Parser Stops
parseStops =
  Stops <$> parseSongStops
  where parseSongStops =
          pound *> stringCI (tagBytes TNStops) *> colon *> sepBy parseStop comma <* semicolon

parseHeader :: Parser Header
parseHeader =
  Header
    <$> parseTitle <* eol
    <*> parseSubtitle <* eol
    <*> parseArtist <* eol
    <*> parseTitleTranslit <* eol
    <*> parseSubtitleTranslit <* eol
    <*> parseArtistTranslit <* eol
    <*> parseGenre <* eol
    <*> parseCredit <* eol
    <*> parseBanner <* eol
    <*> parseBackground <* eol
    <*> parseLyricsPath <* eol
    <*> parseCDTitle <* eol
    <*> parseMusic <* eol
    <*> parseOffset <* eol
    <*> parseBPMChanges <* eol
    <*> parseStops <* eol
    <*> parseSampleStart <* eol
    <*> parseSampleLength <* eol


-- #BACKGROUND:;
parseBGChanges :: Parser BGChanges
parseBGChanges = undefined

-- parseKeySounds :: Parser KeySounds
-- parseKeySounds = undef

parseNotes :: Parser Notes
parseNotes = undefined
